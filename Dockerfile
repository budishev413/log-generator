# FROM python:3.9.0-slim

# WORKDIR /app
# COPY . .

# ENTRYPOINT [ "python3", "gen.py" ]
# CMD [ "--minSleepMs 36000", "--maxSleepMs 300000", "--sourceDataFile defaultDataFile.txt" "--iterations -1", "--minLines 1", "--maxLines 3" ]

FROM alpine:3.15
RUN apk add --no-cache bc
WORKDIR /app
COPY loggenerator.sh /app/
RUN ["chmod", "+x", "loggenerator.sh"]
ENTRYPOINT ["sh", "loggenerator.sh"]
CMD [ "100", "4600" ]